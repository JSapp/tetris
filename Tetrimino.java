import java.awt.Color;
import java.awt.Point;
import java.util.Random;

/**
 * Class representing a tetris piece
 */
public class Tetrimino
{

    private Color color;
    private int gridH, gridW;
    private int[][] position;
    private Point origin;
    private String shape;

    /**
     * Constructor
     *
     * @param gridHeight height of grid
     * @param gridWidth width of grid
     */
    public Tetrimino(int gridHeight, int gridWidth)
    {
        gridH = gridHeight;
        gridW = gridWidth;
        reset();
    }// Tetrimino

    /**
    * Used for collision
    */
    public int[][] checkRotate()
    {
        int[][] currentPosition = new int[position.length][position[0].length];
        for(int i = 0; i < currentPosition.length; i++)
            for(int j = 0; j < currentPosition[0].length; j++)
            	currentPosition[i][j] = position[i][j];

        rotate();

        int[][] futurePosition = new int[position.length][position[0].length];
        for(int i = 0; i < futurePosition.length; i++)
            for(int j = 0; j < futurePosition[0].length; j++)
            	futurePosition[i][j] = position[i][j];


        position = currentPosition;

        return futurePosition;

    }// checkReset


    /**
     * Return a block's color
     */
    public Color getColor()
    {
        return color;
    }//getColor

    /**
     * Returns a block's coordinates
     * @return position the block's coordinates
     */
    public int[][] getCoord()
    {
        return position;
    }// getCoord

    /**
     * Determines if a block has hit the floor
     * @return hit the block's status
     */
    public boolean hitBottom()
    {
        boolean hit = false;
        for(int i = 0; i < position[0].length; i++)
        {
            if(position[position.length - 1][i] == 1) hit = true;
        }
        return hit;
    }// hitBottom

    /**
     * Moves the block down one space
     */
    public void moveDown()
    {
        boolean cancel = false;
        for(int i = position.length - 1; i >= 0; i--)
        {
            for(int j = 0; j < position[0].length; j++)
            {
                if(i == position.length - 1 && position[i][j] == 1)
                {
                    cancel = true;
                    break;
                }
                if(position[i][j] == 1)
                {
                    position[i + 1][j] = 1;
                    position[i][j] = 2; //A 2 represents a space scheduled for erasing
                }
            }
            if(cancel == true) break;
        }
        if(cancel == false) origin.translate(1, 0);
    }// moveDown

    /**
     * Moves the block left one space
     */
    public void moveLeft()
    {
        boolean cancel = false;
        for(int i = 0; i < position[0].length; i++)
        {
            for(int j = 0; j < position.length; j++)
            {
                if(i == 0 && position[j][i] == 1)
                {
                    cancel = true;
                    break;
                }
                if(position[j][i] == 1)
                {
                    position[j][i - 1] = 1;
                    position[j][i] = 2; //A 2 represents a space scheduled for erasing
                }
            }
            if(cancel == true) break;
        }
        if(cancel == false) origin.translate(0, -1);
    }// moveLeft

    /**
     * Moves the block right one space
     */
    public void moveRight()
    {
        boolean cancel = false;
        for(int i = position[0].length - 1; i >= 0; i--)
        {
            for(int j = 0; j < position.length; j++)
            {
                if(i == position[0].length - 1 && position[j][i] == 1)
                {
                    cancel = true;
                    break;
                }
                if(position[j][i] == 1)
                {
                    position[j][i + 1] = 1;
                    position[j][i] = 2; //A 2 represents a space scheduled for erasing
                }
            }
            if(cancel == true) break;
        }
        if(cancel == false) origin.translate(0, 1);
    }// moveRight

    /**
     * Creates a new shape, abandoning the old one to the cruel world
     */
    public void reset()
    {
    	/* Reset position */;
    	position = new int[gridH][gridW];
    	
    	Random randomGenerator = new Random();
        int randomNumber = randomGenerator.nextInt(7);

        switch(randomNumber)
        {
        	case 0:
        		shape = "I";
        		color = Color.cyan;
        		position[0][3] = 1;
                position[0][4] = 1;
                position[0][5] = 1;
                position[0][6] = 1;
                origin = new Point(0, 4);
        		break;
        	case 1:
        		shape = "L";
        		color = Color.orange;
        		position[1][4] = 1;
                position[1][5] = 1;
                position[1][6] = 1;
                position[0][6] = 1;
                origin = new Point(1, 5);
        		break;
        	case 2:
        		shape = "J";
        		color = Color.blue;
        		position[1][3] = 1;
                position[1][4] = 1;
                position[1][5] = 1;
                position[0][3] = 1;
                origin = new Point(1, 4);
        		break;
        	case 3:
        		shape = "T";
        		color = Color.magenta;
        		position[1][3] = 1;
                position[1][4] = 1;
                position[1][5] = 1;
                position[0][4] = 1;
                origin = new Point(1, 4);
        		break;
        	case 4:
        		shape = "Z";
        		color = Color.red;
        		position[2][4] = 1;
                position[1][4] = 1;
                position[1][5] = 1;
                position[0][5] = 1;
                origin = new Point(1, 4);
        		break;
        	case 5:
        		shape = "S";
        		color = Color.green;
        		position[2][5] = 1;
                position[1][4] = 1;
                position[1][5] = 1;
                position[0][4] = 1;
                origin = new Point(1, 5);
        		break;
        	case 6:
        		shape = "O";
        		color = Color.yellow;
        		position[1][4] = 1;
                position[1][5] = 1;
                position[0][4] = 1;
                position[0][5] = 1;
                origin = new Point(0,0);
        		break;
        } // switch
    }// reset

    /**
     * Rotates a block about its origin
     */
    public void rotate()
    {
    	/* Turn away, youths, lest ye face the dreaded hardcoded monster */
        boolean cancel = false;
        
        for(int i = 0; i < position.length; i ++)
        {
            for(int j = 0; j < position[0].length; j++)
            {
                if(shape.equals("I") )
                {
                    if(position[(int)origin.getX()][(int)origin.getY() - 1] == 1)
                    {
                        position[(int)origin.getX()][(int)origin.getY() - 1] = 2;
                        position[(int)origin.getX()][(int)origin.getY() + 1] = 2;
                        position[(int)origin.getX()][(int)origin.getY() + 2] = 2;
                        position[(int)origin.getX() - 1][(int)origin.getY()] = 1;
                        position[(int)origin.getX() + 1][(int)origin.getY()] = 1;
                        position[(int)origin.getX() + 2][(int)origin.getY()] = 1;
                    } // if
                    else
                    {
                        position[(int)origin.getX() - 1][(int)origin.getY()] = 2;
                        position[(int)origin.getX() + 1][(int)origin.getY()] = 2;
                        position[(int)origin.getX() + 2][(int)origin.getY()] = 2;
                        position[(int)origin.getX()][(int)origin.getY() - 1] = 1;
                        position[(int)origin.getX()][(int)origin.getY() + 1] = 1;
                        position[(int)origin.getX()][(int)origin.getY() + 2] = 1;
                    } // else
                    cancel = true;
                } // if
                else if(shape.equals("L"))
                {
                    if(position[(int)origin.getX() - 1][(int)origin.getY() + 1] == 1)
                    {
                        position[(int)origin.getX() + 1][(int)origin.getY() + 1] = 1;
                        position[(int)origin.getX() + 1][(int)origin.getY()] = 1;
                        position[(int)origin.getX() - 1][(int)origin.getY()] = 1;
                        position[(int)origin.getX() - 1][(int)origin.getY() + 1] = 2;
                        position[(int)origin.getX()][(int)origin.getY() + 1] = 2;
                        position[(int)origin.getX()][(int)origin.getY() - 1] = 2;
                    } // if
                    else if(position[(int)origin.getX() + 1][(int)origin.getY() + 1] == 1)
                    {
                        position[(int)origin.getX()][(int)origin.getY() + 1] = 1;
                        position[(int)origin.getX()][(int)origin.getY() - 1] = 1;
                        position[(int)origin.getX() + 1][(int)origin.getY() - 1] = 1;
                        position[(int)origin.getX() - 1][(int)origin.getY()] = 2;
                        position[(int)origin.getX() + 1][(int)origin.getY()] = 2;
                        position[(int)origin.getX() + 1][(int)origin.getY() + 1] = 2;
                    } // else if
                    else if(position[(int)origin.getX() + 1][(int)origin.getY() - 1] == 1)
                    {
                        position[(int)origin.getX() - 1][(int)origin.getY() - 1] = 1;
                        position[(int)origin.getX() - 1][(int)origin.getY()] = 1;
                        position[(int)origin.getX() + 1][(int)origin.getY()] = 1;
                        position[(int)origin.getX() + 1][(int)origin.getY() - 1] = 2;
                        position[(int)origin.getX()][(int)origin.getY() + 1] = 2;
                        position[(int)origin.getX()][(int)origin.getY() - 1] = 2;
                    } // else if
                    else
                    {
                        position[(int)origin.getX()][(int)origin.getY() - 1] = 1;
                        position[(int)origin.getX()][(int)origin.getY() + 1] = 1;
                        position[(int)origin.getX() - 1][(int)origin.getY() + 1] = 1;
                        position[(int)origin.getX() - 1][(int)origin.getY() - 1] = 2;
                        position[(int)origin.getX() - 1][(int)origin.getY()] = 2;
                        position[(int)origin.getX() + 1][(int)origin.getY()] = 2;
                    } // else
                    cancel = true;
                } // else if
                else if(shape.equals("J"))
                {
                    if(position[(int)origin.getX() - 1][(int)origin.getY() - 1] == 1)
                    {
                        position[(int)origin.getX() - 1][(int)origin.getY() + 1] = 1;
                        position[(int)origin.getX() - 1][(int)origin.getY()] = 1;
                        position[(int)origin.getX() + 1][(int)origin.getY()] = 1;
                        position[(int)origin.getX() - 1][(int)origin.getY() - 1] = 2;
                        position[(int)origin.getX()][(int)origin.getY() - 1] = 2;
                        position[(int)origin.getX()][(int)origin.getY() + 1] = 2;
                    } // if
                    else if(position[(int)origin.getX() - 1][(int)origin.getY() + 1] == 1)
                    {
                        position[(int)origin.getX()][(int)origin.getY() - 1] = 1;
                        position[(int)origin.getX()][(int)origin.getY() + 1] = 1;
                        position[(int)origin.getX() + 1][(int)origin.getY() + 1] = 1;
                        position[(int)origin.getX() - 1][(int)origin.getY() + 1] = 2;
                        position[(int)origin.getX() - 1][(int)origin.getY()] = 2;
                        position[(int)origin.getX() + 1][(int)origin.getY()] = 2;
                    } // else if
                    else if(position[(int)origin.getX() + 1][(int)origin.getY()  + 1] == 1)
                    {
                        position[(int)origin.getX() - 1][(int)origin.getY()] = 1;
                        position[(int)origin.getX() + 1][(int)origin.getY()] = 1;
                        position[(int)origin.getX() + 1][(int)origin.getY() - 1] = 1;
                        position[(int)origin.getX() + 1][(int)origin.getY() + 1] = 2;
                        position[(int)origin.getX()][(int)origin.getY() - 1] = 2;
                        position[(int)origin.getX()][(int)origin.getY() + 1] = 2;
                    } // else if
                    else
                    {
                        position[(int)origin.getX() - 1][(int)origin.getY() - 1] = 1;
                        position[(int)origin.getX()][(int)origin.getY() - 1] = 1;
                        position[(int)origin.getX()][(int)origin.getY() + 1] = 1;
                        position[(int)origin.getX() + 1][(int)origin.getY() - 1] = 2;
                        position[(int)origin.getX() - 1][(int)origin.getY()] = 2;
                        position[(int)origin.getX() + 1][(int)origin.getY()] = 2;
                    } // else
                    cancel = true;
                } // else if
                else if(shape.equals("T"))
                {
                    if(position[(int)origin.getX() + 1][(int)origin.getY()] == 0 || position[(int)origin.getX() + 1][(int)origin.getY()] == 2)
                    {
                        position[(int)origin.getX() + 1][(int)origin.getY()] = 1;
                        position[(int)origin.getX()][(int)origin.getY() - 1] = 2;
                    } // if
                    else if(position[(int)origin.getX()][(int)origin.getY() - 1] == 0 || position[(int)origin.getX()][(int)origin.getY() - 1] == 2)
                    {
                        position[(int)origin.getX()][(int)origin.getY() - 1] = 1;
                        position[(int)origin.getX() - 1][(int)origin.getY()] = 2;
                    } // else if
                    else if(position[(int)origin.getX() - 1][(int)origin.getY()] == 0 || position[(int)origin.getX() - 1][(int)origin.getY()] == 2)
                    {
                        position[(int)origin.getX() - 1][(int)origin.getY()] = 1;
                        position[(int)origin.getX()][(int)origin.getY() + 1] = 2;
                    } // else if
                    else
                    {
                        position[(int)origin.getX()][(int)origin.getY() + 1] = 1;
                        position[(int)origin.getX() + 1][(int)origin.getY()] = 2;
                    } // else
                    cancel = true;
                } // else if
                else if(shape.equals("Z"))
                {
                    if(position[(int)origin.getX()][(int)origin.getY() - 1] == 0 || position[(int)origin.getX()][(int)origin.getY() - 1] == 2)
                    {
                        position[(int)origin.getX()][(int)origin.getY() - 1] = 1;
                        position[(int)origin.getX() + 1][(int)origin.getY() + 1] = 1;
                        position[(int)origin.getX() - 1][(int)origin.getY() + 1] = 2;
                        position[(int)origin.getX()][(int)origin.getY() + 1] = 2;
                    } // if
                    else
                    {
                        position[(int)origin.getX() - 1][(int)origin.getY() + 1] = 1;
                        position[(int)origin.getX()][(int)origin.getY() + 1] = 1;
                        position[(int)origin.getX()][(int)origin.getY() - 1] = 2;
                        position[(int)origin.getX() + 1][(int)origin.getY() + 1] = 2;
                    } // else
                    cancel = true;
                } // else if
                else if(shape.equals("S"))
                {
                    if(position[(int)origin.getX()][(int)origin.getY() + 1] == 0 || position[(int)origin.getX()][(int)origin.getY() + 1] == 2)
                    {
                        position[(int)origin.getX()][(int)origin.getY() + 1] = 1;
                        position[(int)origin.getX() + 1][(int)origin.getY() - 1] = 1;
                        position[(int)origin.getX() - 1][(int)origin.getY() - 1] = 2;
                        position[(int)origin.getX()][(int)origin.getY() - 1] = 2;
                    } // if
                    else
                    {
                        position[(int)origin.getX() - 1][(int)origin.getY() - 1] = 1;
                        position[(int)origin.getX()][(int)origin.getY() - 1] = 1;
                        position[(int)origin.getX()][(int)origin.getY() + 1] = 2;
                        position[(int)origin.getX() + 1][(int)origin.getY() - 1] = 2;
                    } // else
                    cancel = true;
                } // else if
                if(cancel == true)
                	break;
            } // for
            if(cancel == true)
            	break;
        } // for
    }// rotate

}// Tetrimino