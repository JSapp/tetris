import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;

public class Driver {

    public static void createAndShowGUI() {

        JFrame frame = new JFrame("Tetris of Doom");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // create a panel
        TetrisPanel panel = new TetrisPanel();

        // add the panel to the frame
        frame.add(panel);

        // show the window.
        frame.pack();
        frame.setVisible(true);

    } // createAndShowGUI

    public static void main(String[] args) {

        // Schedule a job for the event dispatch thread:
        // creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    createAndShowGUI();
                } // run
            });

    } // main

} // Driver