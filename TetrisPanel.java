import java.awt.Dimension;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * The primary container of the tetris game
 */
public class TetrisPanel extends JPanel implements  ActionListener, KeyListener
{

    private boolean done = false, rightPressed = false, leftPressed = false, downPressed = false, rotateAble = true;
    private int size = 20; // Square size
    private int delay = 1000; //Game speed (Difficulty)
    private int[][] board = new int[size][size/2];
    private Color[][] boardColor = new Color[size][size/2];
    private JButton restartButton;
    private JLabel score;
    private Tetrimino currentShape;
    private Timer timer, focusTimer;

    /**
     * Constructor
     */
    public TetrisPanel()
    {
        //Panel values
        setPreferredSize(new Dimension(size * 10, size * 20));

        //Score
        score = new JLabel("0");
        add(score);

        //Restart
        restartButton = new JButton("Restart?");
        restartButton.setVisible(false);
        restartButton.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    done = false;
                    score.setText("0");
                    restartButton.setVisible(false);
                    timer.start();
                    focusTimer.start();
                }// actionPerformed
            });
        add(restartButton);

        //Key setup
        setFocusable(true);
        addKeyListener(this);

        //Game timer
        timer = new Timer(delay, this);
        timer.start();

        //Focus timer
        focusTimer = new Timer(1000/12, new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    requestFocus();
                    if(rightPressed == true && leftPressed == false && checkRight() == true) currentShape.moveRight();
                    if(rightPressed == false && leftPressed == true && checkLeft() == true) currentShape.moveLeft();
                    if(downPressed == true && checkBottom() == true) currentShape.moveDown();
                    repaint();
                }// actionPerformed
            });
        focusTimer.start();

        //Test Stuff
        currentShape = new Tetrimino(size, size/2);

    }// TetrisPanel

    /**
     * Checks the bottom boundary of the piece
     */
    public boolean checkBottom()
    {
        boolean able = true;

        for(int i = 0; i < board.length - 1; i++)
            for(int j = 0; j < board[0].length; j++)
                if(currentShape.getCoord()[i][j] == 1 && currentShape.getCoord()[i + 1][j] == 0 && board[i + 1][j] == 1) able = false;
 
        return able;
    }// checkBottom

    /**
     * Checks the left boundary of the piece
     */
    public boolean checkLeft()
    {
        boolean able = true;
        
        for(int i = 0; i < board.length; i++)
            for(int j = 1; j < board[0].length; j++)
                if(currentShape.getCoord()[i][j] == 1 && currentShape.getCoord()[i][j - 1] == 0 && board[i][j - 1] == 1)
                	able = false;

        return able;
    }// checkLeft

    /**
     * Checks the right boundary of the piece
     */
    public boolean checkRight()
    {
        boolean able = true;
        
        for(int i = 0; i < board.length; i++)
            for(int j = 0; j < board[0].length - 1; j++)
                if(currentShape.getCoord()[i][j] == 1 && currentShape.getCoord()[i][j + 1] == 0 && board[i][j + 1] == 1)
                	able = false;

        return able;
    }// checkRight

    /**
     * Checks for complete rows, and clears them
     */
    public void clearLine()
    {
        int count = 0;
        for(int i = 0; i < board.length; i++)
            for(int j = 0; j < board[0].length; j++)
            {
                if(board[i][j] == 0) break;
                if(j == board[0].length - 1)
                {
                    count += 1;
                    for(int k = 0; k < board[0].length; k++)
                        board[i][k] = 0;
                } // if
            } // for

        if(count > 1) count = (int)Math.pow(2, count);
        score.setText(Integer.toString(Integer.parseInt(score.getText()) + count));
        dropBoard();
    }// clearLine

    /**
     * Moves all blocks down if a line is cleared below
     */
    public void dropBoard()
    {
        for(int i = 0; i < board.length; i++)
            for(int j = 0; j < board[0].length; j++)
            {
                if(board[i][j] == 1) break;
                if(j == board[0].length - 1)
                    for(int k = i; k > 0; k--)
                        for(int l = 0; l < board[0].length; l++)
                            board[k][l] = board[k - 1][l];
            } // for
        
        /* Increase difficulty with each clear */
        if(delay > 250)
        	delay -= 10;
        
        timer.setDelay(delay);
    }// dropBoard

    /**
     * Override for the game loop
     */
    public void actionPerformed(ActionEvent e)
    {
        if(currentShape.hitBottom() == true || checkBottom() == false)
        {
            clearLine();
            currentShape.reset();
            for(int i = 0; i < board[1].length; i++)
            {
                if(board[1][i] == 1 && currentShape.getCoord()[1][i] == 1) done = true;
            } // for
        } // if
        if(downPressed == false) currentShape.moveDown();
    }// actionEvent

    /**
     * Override for custom graphics
     */
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);

        /* Adjust board for active Tetrimino */
        for(int i = 0; i < board.length; i++)
            for(int j = 0; j < board[0].length; j++)
            {
                if(currentShape.getCoord()[i][j] == 1)
                {
                	board[i][j] = 1;
                	boardColor[i][j] = currentShape.getColor();
                } // if
                else if(currentShape.getCoord()[i][j] == 2)
                	board[i][j] = 0;
            } // for

        /* Display board */
        for(int i = 0; i  < board.length; i++)
            for(int j = 0; j < board[0].length; j++)
                if(board[i][j] == 1)
                {
                	g.setColor(boardColor[i][j]);
                	g.fillRect(j * size, i * size, size, size);
                	g.setColor(Color.gray);
                } // if

        /* Defeat */
        if(done == true)
        {
            super.paintComponent(g);
            board = new int[size][size/2];
            timer.stop();
            focusTimer.stop();
            delay = 1000;
            restartButton.setVisible(true);
        } // if

    }// paintComponent

    /**
     * Override for keyboard support
     */
    public void keyTyped(KeyEvent e)
    {
    }// keyTyped

    /**
     * Override for keyboard support
     */
    public void keyPressed(KeyEvent e)
    {
    	/* Move Tetrimino down */
        if(e.getKeyCode() == 40)
        	downPressed = true;
        
        /* Move Tetrimino right */
        if(e.getKeyCode() == 39)
        	rightPressed = true;
        
        /* Move Tetrimino left */
        if(e.getKeyCode() == 37)
        	leftPressed = true;
        
        /* Rotate Tetrimino */
        if(e.getKeyCode() == 38 && rotateAble == true)
        {
            boolean cancel = false;
            for(int i = 0; i < board.length; i++)
            {
                for(int j = 0; j < board[0].length; j++)
                    if(currentShape.checkRotate()[i][j] == 1 && board[i][j] == 1 && currentShape.getCoord()[i][j] == 0)
                    	cancel = true;
            } // for
            
            if(cancel == false)
            {
                currentShape.rotate();
                rotateAble = false;
            } // for
        } // if
        
    }// keyPressed

    /**
     * Override for keyboard support
     */
    public void keyReleased(KeyEvent e)
    {
        if(e.getKeyCode() == 40) downPressed = false;
        if(e.getKeyCode() == 39) rightPressed = false;
        if(e.getKeyCode() == 38) rotateAble = true; //This line doesn't work right now. Java has some weird definitions of 'key released'
        if(e.getKeyCode() == 37) leftPressed = false;
    }// keyReleased

}// TetrisPanel